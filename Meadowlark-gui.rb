require 'discordrb'
require 'tk'
require 'os'
require 'clipboard'
require_relative 'TKTextIO.rb'

class MeadowlarkGui
  def initialize(hash={})
    @bot = Discordrb::Bot.new(token: hash[:token], 
                           client_id: hash[:client_id]
                          )
                          
    @current_server = hash[:server]
    @current_channel = hash[:channel]
    
	if hash.keys.include? :title
		titleStr = hash[:title]
	else
		titleStr = "Meadowlark Discord"
    end
    @root = TkRoot.new {title titleStr}
    
	if hash.keys.include? :icon
		iconpath = hash[:icon]
	else
		iconpath = './meadowlark.gif'
    end
	myicon = TkPhotoImage.new('file' => iconpath)
	
	@root.iconphoto(myicon)
    
    @root.geometry('1225x594')
    @channelPanel = Tk::Tile::Frame.new(@root) {}.grid( :column=>0,:row=>0,
                                                                       :sticky=>'nsew',
                                                                       :columnspan=>2
                                                                     )
    TkGrid.columnconfigure @root, 0, :weight => 1; TkGrid.rowconfigure @root, 0, :weight => 1

    col1width = '130'

    @messagePanel = Tk::Text.new(@channelPanel, {height: '35',
                                                width: col1width,
                                                bg: "#393c41", 
                                                fg: "#ffffff",
                                                #wrap: 'word',
                                                #text: "test" 
                                                }) {}.grid( :sticky => 'w')

    @messagePanel.tag_configure('backload', :foreground=> "#edd400", :relief=>'raised')
    @messagePanel.tag_bind('backload', '1', proc{ backloadMessages })
    @messagePanel.insert(1.0, "================== more ==================\n", 'backload')
    
    @messagePanel.tag_configure('image-link', :foreground=> "#4f8fcf", :relief=>'raised')
    @messagePanel.tag_bind('image-link', '1', proc{|event| copy_link(event, @messagePanel) })
    
    @messagePanel.state = 'disabled'
    
    

    @consolePanel = Tk::Text.new(@channelPanel, { height: '35',
                                            width: '40',
                                            bg: "#393c41", 
                                            fg: "#ffffff",
                                            #wrap: 'word',
                                            #text: "test" 
                                            }) {}.grid(:column=>1, :row=>0, 
                                                       :sticky => 'nw',
                                                       :padx => 5,
                                                       :pady => 5)
               
    @consolePanel.tag_configure('green', foreground: "#8ae234")
    @consolePanel.tag_configure('yellow', foreground: "#ffff00")
    @consolePanel.tag_configure('emoji', foreground: "#8ae234")
    @consolePanel.tag_configure('server', foreground: "#8ae234")
    @consolePanel.tag_configure('channel', foreground: "#8ae234")
    @consolePanel.tag_bind('emoji', '1', proc{|event| appendToInputPanel(event) })
    @consolePanel.tag_bind('server', '1', proc{
          |event| resolveServerID( lineFromConsoleClick(event).strip.to_i ) 
        })
    @consolePanel.tag_bind('channel', '1', proc{
          |event| resolveChannelID( lineFromConsoleClick(event).strip.to_i ) 
        })
        
    @commandsPanel = Tk::Text.new(@channelPanel, { height: '5',
                                            width: '40',#'28',
                                            bg: "#393c41", 
                                            fg: "#ffffff",
                                            #wrap: 'word',
                                            #font: TkFont.new(size: 11,weight: 'bold')
                                            }) {}.grid(:column=>1, :row=>1, 
                                                       :sticky => 'nw',
                                                       :padx => 5,
                                                       :pady => 5)
                                                       
    @commandsPanel.tag_configure('refresh', :foreground=> "#bd5c05", 
                                 font: TkFont.new(size: 11,weight: 'bold'))#"#edd400")
    @commandsPanel.tag_bind('refresh', '1', proc{ refreshMessagePanel })
    @commandsPanel.tag_configure('green', foreground: "#8ae234")

    @inputPanel = TkTextIO.new(@channelPanel, { height: '5',
                                              width: col1width,
                                              bg: "#393c41", 
                                              fg: "#ffffff",
                                              #wrap: 'word',
                                              :mode=>:console,
                                              #:prompt_cmd=>proc{
                                              #  cmdline(num) #"[#{num+=1}]"
                                              #},
                                              :prompt=>''
                                             }) {}.grid( :column=>0,:row=>1,:sticky => 'w')

    TkWinfo.children(@channelPanel).each {|w| TkGrid.configure w, :padx => 5, :pady => 5}
    
    textPanelWrite(@consolePanel,"Meadowlark's invite URL:\n#{@bot.invite_url}\n", 'green')
  
    

    @bot.run(async = :async)
    @bot.game=("with woodland creatures")
    
    resolveServerID(@current_server)
    resolveChannelID(@current_channel)
    
    @bot.message do |message|
      #puts message.author.username
      if message.channel == @current_channel.id
        displayMessage(message.author.username, message.content)
      end
    end
    
    updateCommandsPanel
    @ev_loop = Thread.new{Tk.mainloop}
  end
  
  attr_accessor :current_server, :current_channel, :bot, :inputPanel
  
  def copy_link(event, panel = @consolePanel)
    Clipboard.copy( lineFromConsoleClick(event, panel).split(" ")[0] )
  end
  
  def copy_text(event, panel = @consolePanel)
    Clipboard.copy( lineFromConsoleClick(event, panel) )
  end
  
  def resolveServerID(id)
    @bot.servers.each do |server_id, server|
      if id == server_id
        @current_server = server
        self.updateCommandsPanel
        self.parse("/chan list")
        #refreshMessagePanel
        return
      end
    end
  end
  
  def resolveChannelID(id)
    if @current_server == nil
	  return
	end
	
	@current_server.text_channels.each do |chan|
      if chan.id == id
        @current_channel = chan
        self.refreshMessagePanel
        self.updateCommandsPanel
        self.displayEmojiList
        return
      end
    end
  end
  
  def postMessage(msg)
    @current_channel.send_message(msg)
  end
  
  def postTemporaryMessage(msg, timeout=1)
    @current_channel.send_temporary_message(msg, timeout)
  end
  
  def textPanelWrite(panel, text, tag = nil)
    panel.state = 'normal'
    panel.delete(1.0,'end')
    panel.insert(1.0, "#{text}\n", tag)
    panel.state = 'disabled'
  end

  def textPanelAppend(panel, text, tag = nil)
    panel.state = 'normal'
    panel.insert('end', "#{text}\n", tag)
    panel.state = 'disabled'
  end
  
  def lineFromConsoleClick(event, panel = @consolePanel)
    first = panel.index("@#{event.x},#{event.y} linestart")
    last =  panel.index("@#{event.x},#{event.y} lineend")
    panel.get(first,last)
  end
  
  def appendToInputPanel(event)
    @inputPanel.insert('end', lineFromConsoleClick(event))
  end
  
  def updateCommandsPanel
    @commandsPanel.state = 'normal'
    @commandsPanel.delete(1.0, 'end')
    if @current_server != nil and @current_channel != nil
      @commandsPanel.insert(1.0, "#{@current_server.name}\n  \##{@current_channel.name}\n\n", 'green')
    else
      @commandsPanel.insert(1.0, "type \"/server list\"\n\n")
    end
    #cmdPanel.insert('end', "Current Channel:  #{channel}\n", 'green')
    @commandsPanel.insert('end', "refresh\n", 'refresh')
    @commandsPanel.state = 'disabled'
  end
  
  def displayMessage(author, msg, attachment_urls = [])
    str = "#{author.rjust(10)[0..9]}:  #{msg}\n"
    urls = ""
    attachment_urls.each do |url|
      urls += "             #{url}\n"
    end
    uris = URI.extract(msg)
    if uris.length > 0
      uris.each do |uri|
        urls += "             #{uri}\n"
      end
    end
    @messagePanel.state = "normal"
    @messagePanel.insert('end', str)
    if urls != ""
      @messagePanel.insert('end', urls, 'image-link')
    end
    @messagePanel.state = "disabled"
  end
  
  def displayMessageInsert(author, msg, attachment_urls = [])
    str = "#{author.rjust(10)[0..9]}:  #{msg}\n"
    urls = ""
    attachment_urls.each do |url|
      urls += "             #{url}\n"
    end
    uris = URI.extract(msg)
    if uris.length > 0
      uris.each do |uri|
        urls += "             #{uri}\n"
      end
    end
    @messagePanel.state = "normal"
    if urls != ""
      @messagePanel.insert('2.0', urls, 'image-link')
    end
    
    @messagePanel.insert('2.0', str)
    @messagePanel.state = "disabled"
  end
  
  def refreshMessagePanel
    @messagePanel.state = "normal"
    @messagePanel.delete(1.0, 'end')
    @messagePanel.insert(1.0, "================== more ==================\n", 'backload')
    @messagePanel.state = "disabled"
    
    @current_history_message_id = nil
    backloadMessages(20)
    ##################################
    #@messagePanel.state = "normal"
    ##################################
  end
  
  def backloadMessages(count=20)
    history = @current_channel.history(count, @current_history_message_id)
    history.each do |message|
      if message.attachments.length > 0
         urls = []
         message.attachments.each do |attachment|
            urls << attachment.url
         end
         displayMessageInsert(message.author.username, message.content, urls)
      else
        displayMessageInsert(message.author.username, message.content)
      end
    end
    @current_history_message_id = history[-1].id
  end
  
  def displayEmojiList
    #textPanelWrite(@consolePanel, "")
    @consolePanel.state = 'normal'
    @consolePanel.delete(1.0, 'end')
    @bot.servers.each do |id, server|
		textPanelAppend(@consolePanel, "#{server.name}:\n")
		emotes = []
		server.emoji.values.each do |emote|
		  emotes << emote.mention
		end
		
		emotes.sort.each do |emote|
	        textPanelAppend(@consolePanel, emote, 'emoji')
	    end
    end
    @consolePanel.state = 'disabled'
  end
  
  def parse(msg)
    if msg[0] != '/'
      displayMessage(@bot.profile.username, msg)
      postMessage(msg)
    else
      sentence = msg.downcase.split(' ')
      #puts sentence[1] == "list"
      cmd = sentence[0]
      #puts cmd == "/server"
      if cmd == "/server" or cmd == "/s"
        if sentence[1] == "list"
          #textPanelWrite(@consolePanel, '')
          @consolePanel.state = 'normal'
          @consolePanel.delete(1.0, 'end')
		  
		  servers = {}
		  @bot.servers.each do |id, server|
			servers[server.name] = server.id
		  end
		  
          servers.keys.sort.each do |name|
            #puts server.name
            @consolePanel.insert('end', "#{name}\n", 'yellow')
            @consolePanel.insert('end', "#{servers[name]}\n\n", 'server')
          end
          @consolePanel.state = 'disabled'
          
          #parse("/chan list")
        end
=begin
      elsif cmd == "/dm"
         if sentence[1] == "list"
          #textPanelWrite(@consolePanel, '')
          @consolePanel.state = 'normal'
          @consolePanel.delete(1.0, 'end')
          #puts @current_server.name
		      dms = {}
          @bot.user.user_dms(@bot.token).each do |dm|
              dms[dm.name] = dm.id
          end
		      
		      dms.keys.sort.each do |name|
		        @consolePanel.insert('end', "#{name}\n", 'yellow')
            @consolePanel.insert('end', "#{dms[name]}\n\n", 'channel')
          end
          @consolePanel.state = 'disabled'
        end
=end
      elsif cmd == "/channel" or cmd == "/chan"
        if sentence[1] == "list"
          #textPanelWrite(@consolePanel, '')
          @consolePanel.state = 'normal'
          @consolePanel.delete(1.0, 'end')
          #puts @current_server.name
		  chans = {}
          @current_server.channels.each do |chan|
            
			chans[chan.name] = chan.id
	      end
		  
		  chans.keys.sort.each do |name|
		    @consolePanel.insert('end', "#{name}\n", 'yellow')
            @consolePanel.insert('end', "#{chans[name]}\n\n", 'channel')
          end
          @consolePanel.state = 'disabled'
        end
      elsif cmd == "/emoji" or cmd == "/emote"
        if sentence[1] == "list"
          self.displayEmojiList
        end
      elsif cmd == "/temp"
        displayMessage("$$$temp", sentence[1..-1].join(" "))
		postTemporaryMessage(sentence[1..-1].join(" "))
      end
    end
  end
  
  def run
    @inputThread = Thread.new{
      loop do
        msg = @inputPanel.gets
        @inputPanel.delete(1.0,'end')
        #inputPanel.insert(1.0, "Meadow: ")
        #textPanelAppend(messagePanel, msg)
        #displayMessage("Meadowlark", msg)
        #postMessage(msg)
        parse(msg)
        #puts msg
      end
    }
  end
  
  def join
    @ev_loop.join
	#@inputThread.join
  end
end
